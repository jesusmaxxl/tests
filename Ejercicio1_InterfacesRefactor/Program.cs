﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesRefactor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Llama al servicio -> Servicio hace referencia a quien contiene toda la lógica de negocio de una aplicacion.
            AlertService alert = new AlertService(new AlertDAO());
            Guid id = alert.RaiseAlert();
            Console.WriteLine(alert.GetAlertTime(id));
            Console.ReadKey();
        }
    }
}
