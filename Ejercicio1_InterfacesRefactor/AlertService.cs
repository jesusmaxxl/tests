using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesRefactor
{
    class AlertService
    {
        IAlertDAO _alertDAO;
        
        public AlertService(IAlertDAO _alertDAO)
        {
            this._alertDAO= new AlertDAO();
            _alertDAO.SetDependency(new AlertDAO());
        }

        public Guid RaiseAlert() 
        {
            return _alertDAO.AddAlert(DateTime.Now);
        }

        public DateTime GetAlertTime(Guid id)
        {
            return _alertDAO.GetAlert(id);
        }
    }
}
