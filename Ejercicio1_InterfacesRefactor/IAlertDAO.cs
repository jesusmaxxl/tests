﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesRefactor
{
    interface IAlertDAO
    {
        //Una interfaz es un conjunto de metodos abstractos que obviamente son accesibles y obligan a la clase que lo implenta
        //a tenerlos declarados y se tiene la posibilidad de sobreescribirlo.
        //Tip: Una método abstracto no tiene cuerpo.
        void SetDependency(AlertDAO alertDAO);
        Guid AddAlert(DateTime time);
        DateTime GetAlert(Guid id);
    }
}
