using System;
using System.Linq;

namespace ArregloPrueba1
{
    class ArregloPrueba1
    {
        public static string[] NombreUnico(string[] names1, string[] names2)
        {
            string[] names3 = new string[] { };
            names3 = names1.Union(names2).ToArray();        
            return names3;
        }
        static void Main(string[] args)
        {
            string[] names1 = new string[] { "Luciana", "Juana", "Matilde" };
            string[] names2 = new string[] { "Matilde", "Maria", "Juana" };
            Console.WriteLine(string.Join(", ", NombreUnico(names1, names2)));
        }
    }
}
