﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace extraccion
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account();
            Boolean status = false;
            do {
                Console.WriteLine("Ingrese operacion");
                Console.WriteLine("1- Extracción");
                Console.WriteLine("2- Deposito");
                Console.WriteLine("3- Salir");
                String operation = Console.ReadLine().ToString();
                switch (operation)
                {
                    case "1":
                        String amount = Console.ReadLine();
                        Boolean isWithdrawComplete = account.Withdraw(Convert.ToDouble(amount));
                        if (isWithdrawComplete)
                            Console.WriteLine("Su balance es de: $" + account.balance.ToString() + "\n");
                        else
                            Console.WriteLine("No se pudo realizar la extracción");
                        break;
                    case "2":
                        if (account.Deposit(100)) 
                            Console.Write("Su deposito se ha hecho correctamente, su balance es de: $" + account.balance.ToString() + "\n");
                        else
                            Console.Write("Su deposito ha sido denegado" + "\n");
                        break;
                    case "3":
                        status = true;
                        break;
                    default:
                        Console.WriteLine("Ingrese un valor correcto.\n");
                        break;
                }
            } while (!status);
            Console.ReadKey();
        }
    }
}
