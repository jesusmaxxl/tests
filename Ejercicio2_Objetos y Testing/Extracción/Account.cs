using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace extraccion
{
    public class Account
    {
        public double balance;
        public const double overdraftLimit = 3000;

        public Boolean Deposit(double money)
        {
            if (IsPositive(money))
            {
                this.balance= this.balance + money;
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean Withdraw(double money)
        {
            if (IsPositive(money))
            {
                if (CheckBalance(money)) return ExtractMoney(money);
            }
            return false;
        }

        private Boolean CheckBalance(double money)
        {
            double total = Math.Abs(this.balance - overdraftLimit);
            if (total >= money) return true;
            return false;
        }

        private Boolean IsPositive(double money)
        {
            if (money >= 0) return true;
            return false;

        }

        private Boolean ExtractMoney(double money)
        {
            if (IsOverdrafLimitValid())
            {
                if (HasBalanceLimit(money)) {
                    this.balance -= money;
                    return true;
                }
            }
            return false;
        }

        private Boolean IsOverdrafLimitValid()
        {
            if (this.balance > (3000 * -1)) return true;
            return false;
        }

        private Boolean HasBalanceLimit(double money)
        {
            Double total = 0;
            if (!IsPositive(this.balance))
                total = Math.Abs(this.balance) - overdraftLimit;
            else
                total = this.balance - (overdraftLimit * -1);

            if (total >= money) return true;
            return false;
        }

    }
}
