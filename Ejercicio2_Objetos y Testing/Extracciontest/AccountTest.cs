﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using extraccion;

namespace extracciontest
{
    [TestClass]
    public class AccountTest
    {
        [TestMethod]
        public void IsDepositeFalseWhenNumberIsNegative()
        {
            Account account = new Account();
            var result = account.Deposit(-300);
            Assert.IsFalse(result, "El deposito no se pudo realizar");
        }

        [TestMethod]
        public void IsDepositeTrueWhenNumberIsPositive()
        {
            Account account = new Account();
            var result = account.Deposit(300);
            Assert.IsTrue(result, "El deposito se pudo realizar");
        }

        [TestMethod]
        public void IsWithdrawFalseWhenNumberIsNegative()
        {
            Account account = new Account();
            var result = account.Withdraw(-300);
            Assert.IsFalse(result, "La extracción no se pudo realizar");
        }

        [TestMethod]
        public void IsWithdrawtRUEWhenNumberIsPositive()
        {
            Account account = new Account();
            var result = account.Withdraw(300);
            Assert.IsTrue(result, "La extracción no se pudo realizar");
        }

        [TestMethod]
        public void IsExtractionFalseWhenMoneyIsGreaterThanOverdraftLimit()
        {
            Account account = new Account();
            Boolean isDepositComplete = account.Deposit(2000);
            Boolean isWithdrawComplete = account.Withdraw(6000);
            Assert.IsFalse(isWithdrawComplete, "La extracción superó el límite");
        }
        
    }
}
